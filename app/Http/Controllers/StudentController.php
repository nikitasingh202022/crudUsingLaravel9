<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Validator;
use App\Models\StudentModel;
use Illuminate\Http\Request;
use DB;
use Symfony\Component\Console\Input\Input;

class StudentController extends Controller
{
    public function insertStudentData(Request $req)
    {
        $validatedData = $req->validate([
            'studentImage' => 'required',
            'studentName' => 'required',
            'studentEmail' => 'required',
            'studentNumber' => 'required',
            'studentDob' => 'required',
            'studentAddress' => 'required'
        ], [
            'studentImage.required' => 'Student Image field is required',
            'studentName.required' => 'studentName field is required.',
            'studentEmail.required' => 'Email field is required.',
            'studentEmail.studentEmail' => 'Email field must be studentEmail address.',
            'studentDob.required' => 'Student Dob field is required',
            'studentAddress.required' => 'Student Address field is required'
        ]);
        if ($validatedData) {
            $studentImage = $req->file('studentImage')->getClientOriginalName();
            $req->studentImage->move(public_path('studentImages'), $studentImage);
            $studentName = $req->get('studentName');
            $studentEmail = $req->get('studentEmail');
            $studentNumber = $req->get('studentNumber');
            $studentDob = $req->get('studentDob');
            $studentAddress = $req->get('studentAddress');
            $obj = new StudentModel();
            $obj->studentImage = $studentImage;
            $obj->studentName = $studentName;
            $obj->studentEmail = $studentEmail;
            $obj->studentNumber = $studentNumber;
            $obj->studentDob = $studentDob;
            $obj->studentAddress = $studentAddress;
            $obj->save();
            return redirect('/');
        }
    }
    public function readData()
    {
        $studentData = StudentModel::all();
        return view('welcome', ['data' => $studentData]);
    }
    public function updateOrDelete(Request $req)
    {
        print_r($_GET);
        die;
        $userId = $req->get('userId');
        $studentImage = $req->get('studentImage');
        $studentName = $req->get('studentName');
        $studentEmail = $req->get('studentEmail');
        $studentNumber = $req->get('studentNumber');
        $studentDob = $req->get('studentDob');
        $studentAddress = $req->get('studentAddress');
        if ($req->get('update') == 'Update') {
            return view('updateView', ['userId' => $userId, 'studentImage' => $studentImage, 'studentEmail' => $studentEmail, 'studentName' => $studentName, 'studentNumber' => $studentNumber, 'studentDob' => $studentDob, 'studentAddress' => $studentAddress]);
        } else {
            $obj = StudentModel::find($userId);
            $obj->delete();
        }
        return redirect('/');
    }
    public function update(Request $req)
    {
        $userId = $req->get('userId') ? $req->get('userId') : '';
        $studentImage = $req->get('studentImage') ? $req->get('studentImage') : '';;
        $studentName = $req->get('studentName') ? $req->get('studentName') : '';
        $studentEmail = $req->get('studentEmail') ? $req->get('studentEmail') : '';
        $studentNumber = $req->get('studentNumber') ? $req->get('studentNumber') : '';
        $studentDob = $req->get('studentDob') ? $req->get('studentDob') : '';
        $studentAddress = $req->get('studentAddress') ? $req->get('studentAddress') : '';
        $obj = StudentModel::find($userId);
        $obj->studentImage = $studentImage;
        $obj->studentName = $studentName;
        $obj->studentEmail = $studentEmail;
        $obj->studentNumber = $studentNumber;
        $obj->studentDob = $studentDob;
        $obj->studentAddress = $studentAddress;
        $obj->save();
        return redirect('/');

        
    }
}
