<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('insertStudentData', [StudentController::class, 'insertStudentData']);
Route::get('/',[StudentController::class,'readData']);
Route::view('updateView','/updateView');
Route::get('updateOrDelete',[StudentController::class,'updateOrDelete']);
Route::get('update',[StudentController::class,'update']);