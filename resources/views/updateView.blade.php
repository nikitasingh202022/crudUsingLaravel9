<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>

<body>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <div class="col-md-4 m-auto border mt-3 p-2 border-info">
        <h2 class="text-center text-warning">UPdate View</h2>
        <div class="modal-footer">
            <a href="{{URL::to('/')}}" class="btn btn-secondary" data-dismiss="modal">Back</a>
        </div>
        <form action="{{URL::to('update')}}" method="get" enctype="multipart/form-data">
            @csrf
            Student Id <br>
            <input type="number" name="userId" value="{{$userId}}">
            <div class="mb-2">
                <label for="studentImage">Student Image</label>
                <input accept="image/*" type='file' id="studentImage" name="studentImage" class="form-control" />
                <img id="image" src="studentImages{{$studentImage}}" alt=" " width="150" height="160" />
                <img id="output" />
            </div>
            <input type="hidden" id="userId" name="userId">
            <div class="mb-2">
                <label for="studentName">Name</label>
                <input type="text" id="studentName" name="studentName" class="form-control" value="{{$studentName}}" placeholder="Enter Student Name">

            </div>
            <div class="mb-2">
                <label for="studentEmail">Email</label>
                <input type="email" id="studentEmail" name="studentEmail" class="form-control" value="{{$studentEmail}}" placeholder="Enetr Student Email">
            </div>
            <div class="mb-2">
                <label for="studentNumber">Mobile Number</label>
                <input type="number" id="studentNumber" name="studentNumber" class="form-control" value="{{$studentNumber}}" placeholder="Enetr Student Date Of Birth">
            </div>
            <div class="mb-2">
                <label for="studentDob">Date Of Birth</label>
                <input type="date" id="studentDob" name="studentDob" class="form-control" value="{{$studentDob}}" placeholder="Enetr Student Number">
            </div>
            <div class="mb-2">
                <label for="studentAddress">Address</label>
                <textarea name="studentAddress" id="studentAddress" class="form-control" cols="30" rows="3" placeholder="Enetr Student Address">{{$studentAddress}}</textarea>
            </div>

            <button type="submit" class="btn btn-outline-warning">Update</button>


        </form>
    </div>
</body>

</html>