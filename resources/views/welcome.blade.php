<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>

<body>

  <!-- Button trigger modal -->
  <center><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
      Add Student
    </button></center><br><br>
  <div class="container">
    <table class="table mt-5">
      <thead class="bg-danger text-white fw-bold">
        <th>S.N.</th>
        <th>Student Image</th>
        <th>Student Name</th>
        <th>Student Email</th>
        <th>Student Mobile Number</th>
        <th>Student D.O.B</th>
        <th>Student Address</th>
        <th>Update</th>
        <th>Delete</th>
      </thead>
      <tbody class="text-danger bg-light fs-4">
        @foreach($data as $student)
        <tr>
          
          <form action="{{URL::to('updateOrDelete')}}" method="get">
            <td class="pt-5"><input type="hidden" name="userId" value="{{$student['userId']}}">{{$student['userId']}}</td>
            <td class="pt-5"><img style="display: none" src="studentImages/{{$student['studentImage']}}" width="100px" height="100px" alt=""><img src="studentImages/{{$student['studentImage']}}" width="100px" height="100px" alt=""></td>
            <td class="pt-5"><input type="hidden" name="studentName" value="{{$student['studentName']}}"></td>
            <td class="pt-5"><input type="hidden" name="studentEmail" value="{{$student['studentEmail']}}">{{$student['studentEmail']}}</td>
            <td class="pt-5"><input type="hidden" name="studentNumber" value="{{$student['studentNumber']}}">{{$student['studentNumber']}}</td>
            <td class="pt-5"><input type="hidden" name="studentDob" value="{{$student['studentDob']}}">{{$student['studentDob']}}</td>
            <td class="pt-5"><textarea cols="30" rows="3" name="studentAddress" hidden="hidden">{{$student['studentAddress']}}</textarea>{{$student['studentAddress']}}</td>
            <td class="pt-5"><input type="submit" class="btn btn-outline-warning rounded-pill" name="update" value="Update"></td>
            <td class="pt-5"><input type="submit" class="btn btn-outline-danger rounded-pill" name="delete" value="Delete"></td>
          </form>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel" style="color: green;">Student Information</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ URL::to('insertStudentData') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-2">
              <label for="studentImage">Student Image</label>
              <input accept="image/*" type='file' id="studentImage" name="studentImage" class="form-control" />
              <img id="image" src="" alt=" " width="150" height="160" />
              <img id="output" />
              <br><span class="text-danger">
                @error('studentImage')
                {{ $message }}
                @enderror
              </span>

            </div>
            <input type="hidden" id="userId" name="userId">
            <div class="mb-2">
              <label for="studentName">Name</label>
              <input type="text" id="studentName" name="studentName" class="form-control" value="" placeholder="Enter Student Name">
              <br><span class="text-danger">
                @error('studentName')
                {{ $message }}
                @enderror
              </span>

            </div>
            <div class="mb-2">
              <label for="studentEmail">Email</label>
              <input type="email" id="studentEmail" name="studentEmail" class="form-control" value="" placeholder="Enetr Student Email">
              <br><span class="text-danger">
                @error('studentEmail')
                {{ $message }}
                @enderror
              </span>

            </div>
            <div class="mb-2">
              <label for="studentNumber">Mobile Number</label>
              <input type="number" id="studentNumber" name="studentNumber" class="form-control" value="" placeholder="Enetr Student Date Of Birth">
              <br><span class="text-danger">
                @error('studentNumber')
                {{ $message }}
                @enderror
              </span>

            </div>
            <div class="mb-2">
              <label for="studentDob">Date Of Birth</label>
              <input type="date" id="studentDob" name="studentDob" class="form-control" value="" placeholder="Enetr Student Number"> <br><span class="text-danger">
                @error('studentDob')
                {{ $message }}
                @enderror
              </span>

            </div>
            <div class="mb-2">
              <label for="studentAddress">Address</label>
              <textarea name="studentAddress" id="studentAddress" class="form-control" cols="30" rows="3" placeholder="Enetr Student Address"></textarea> <br><span class="text-danger">
                @error('studentAddress')
                {{ $message }}
                @enderror
              </span>

            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
        <!-- <div class="modal-footer">
          <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>

</html>
<script>
  studentImage.onchange = evt => {
    const [file] = studentImage.files
    if (file) {
      image.src = URL.createObjectURL(file)
    }
  }
</script>